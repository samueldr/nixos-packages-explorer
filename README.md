`packages-explorer`
===================

[Demo](https://nix.samueldr.com/explorer/)

Development use
---------------

```
 $ bin/webpack
```

Or with watcher:

```
 $ bin/webpack-dev-server
```
